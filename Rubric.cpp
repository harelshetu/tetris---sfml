#include "Rubric.h"

//constructor
Rubric::Rubric()
	:m_color(sf::Color::Transparent), m_rec_shape(sf::Vector2f(20.0, 20.0)),m_empty(false)
{
	m_rec_shape.setFillColor(m_color);
}
Rubric::Rubric(sf::Color color)
	:m_color(color), m_rec_shape(sf::Vector2f(20.0, 20.0)), m_empty(false) {
	m_rec_shape.setFillColor(m_color);
}

// set is rubric empty or not
void Rubric::set_is_empty(bool empty)
{
	m_empty = empty;
}
// is rubric empty or not
bool Rubric::get_is_empty() const
{
	return m_empty;
}
// get the rubric rectangle
  sf::RectangleShape& Rubric::get_rec_shape()
{
	return m_rec_shape;
}
// draw the rubric
void Rubric::draw(sf::RenderWindow & window)
{
	window.draw(m_rec_shape);
}
// get the color of the rubric
sf::Color Rubric::get_color() const
{
	return m_color;
}

Rubric::~Rubric()
{
}
