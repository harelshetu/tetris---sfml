#include "Controller.h"

Controller::Controller() 
	:m_level(1), m_score(0)
{
	m_window.create(sf::VideoMode::VideoMode(LENGTH * 25, WIDTH * 43), "Tetris");
	rand_shape();
	
}// main function
void Controller::run()
{
	sf::SoundBuffer ring;
	sf::Font font;
	try { // load music and font
		if (!ring.loadFromFile("tetris_music.ogg"))
			throw std::invalid_argument("can not open file");
		if (!font.loadFromFile("david.ttf"))
			throw std::invalid_argument("can not open file");
	}
	catch (std::invalid_argument err)
	{
		return;
	}

	sf::Sound music;
	music.setBuffer(ring);

	sf::Clock valocity_clock, clock;
	float faster = 500;
	sf::Time valocity_stopper, stopper;
	music.setLoop(true);
	music.play();
	while (m_window.isOpen()) // main loop
	{
		if (m_grid.check_game_over())
		{
			music.stop();
			show_game_over(font); // print game over
			faster = 2;
			clock.restart();
			while (1)
			{
				stopper = clock.getElapsedTime();
				if (stopper.asSeconds() > faster)
					break;
			}
			break;
		}

		if (m_shape->getIsLocked())
		{
			rand_shape();
			m_shape->set_is_locked(false);
		}
		m_window.clear();

		m_grid.draw(m_window);
		if (!m_grid.check_game_over())
			m_shape->draw_shape();

		show_score(font);  // print the score and level
		m_window.display();

		stopper = clock.getElapsedTime();
		valocity_stopper = valocity_clock.getElapsedTime();
		sf::Event Event;
		while (m_window.pollEvent(Event))
		{
			switch (Event.type)
			{
			case sf::Event::Resized:
				m_window.setView(sf::View(sf::FloatRect(0, 0, (float)Event.size.width, (float)Event.size.height)));
				break;
			case sf::Event::KeyPressed:
				switch (Event.key.code)
				{
				case sf::Keyboard::Up: // up = rotate the shape
					if (!m_grid.shape_collision(m_shape) &&
						!m_grid.check_left(m_shape) &&
						!m_grid.check_right(m_shape))
						m_shape->rotate_shape();
					break;
				case sf::Keyboard::Down:
					if (!m_grid.shape_collision(m_shape))
						m_shape->move_down();
					else
						m_shape->set_is_locked(true);
					break;
				case sf::Keyboard::Left:
					if (!m_grid.check_left(m_shape))
						m_shape->move_left();
					break;
				case sf::Keyboard::Right:
					if (!m_grid.check_right(m_shape))
						m_shape->move_right();
					break;
				}
				break;
			}
			if (Event.type == sf::Event::Closed)
				m_window.close();
		}
		if (m_grid.get_eaten_row() > 0)
		{
			m_score += 100 * m_grid.get_eaten_row();
			m_grid.set_eaten_row(0);
		}

		if (valocity_stopper.asSeconds() > 15.f) // change valocity
		{
			if (faster > 150)
			{
				m_level++;   //level up
				faster -= 50;  // faster valocity
			}
			valocity_clock.restart();
		}
		if (stopper.asMilliseconds() > faster)
		{
			if (!m_grid.shape_collision(m_shape))
				m_shape->move_down();
			else
				m_shape->set_is_locked(true);

			clock.restart();
		}
		if (m_shape->getIsLocked()) //is shape locked
		{
			m_grid.copy_shape(m_shape);
			m_grid.check_row();
		}
	}
}

// rand shape to the uniqe ptr
void Controller::rand_shape()
{
	int number = rand() % 7 + 1;
	switch (number)
	{
	case 1:
		m_shape = std::make_unique<Shapes>(Line_Shape(m_window));
		break;
	case 2:
		m_shape = std::make_unique<Shapes>(Plus_Shape(m_window));
		break;
	case 3:
		m_shape = std::make_unique<Shapes>(P_Shape(m_window));
		break;
	case 4:
		m_shape = std::make_unique<Shapes>(R_shape(m_window));
		break;
	case 5:
		m_shape = std::make_unique<Shapes>(L_Shape(m_window));
		break;
	case 6:
		m_shape = std::make_unique<Shapes>(Q_Shape(m_window));
		break;
	case 7:
		m_shape = std::make_unique<Shapes>(Square_Shape(m_window));
		break;
	}	
}
// print status
void Controller::show_score(sf::Font const &font)
{
	sf::Text game_score, level;
	std::ostringstream s_score, s_level;
	s_score.str("");
	s_level.str("");
	s_score << "Score: " << m_score;
	s_level << "Level: " << m_level;
	level.setCharacterSize(24);
	game_score.setCharacterSize(24);
	level.setPosition(300, 150);
	game_score.setPosition(300, 200);
	game_score.setFont(font);
	level.setFont(font);
	game_score.setString(s_score.str());
	level.setString(s_level.str());

	m_window.draw(game_score);
	m_window.draw(level);
}
// print game over
void Controller::show_game_over(sf::Font const &font)
{
	m_window.clear();
	sf::Text game_over;
	std::ostringstream game_over_str;
	game_over_str.str("Game Over!");
	game_over.setColor(sf::Color::Red);
	game_over.setCharacterSize(50);
	game_over.setPosition(LENGTH * 7, WIDTH * 15);
	game_over.setFont(font);
	game_over.setString(game_over_str.str());

	m_window.draw(game_over);
	m_window.display();
}

Controller::~Controller()
{}
