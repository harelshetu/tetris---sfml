#include "Grid.h"

//constructor
Grid::Grid()
	:m_eaten_row(0), m_is_game_over(false)
{
	float pos_x = 0, pos_y = 0;
	m_board.resize(LENGTH);

	for (int i = 0; i < LENGTH; i++) //make the board
	{
		for (int j = 0; j < WIDTH; j++)
		{
			Rubric x = Rubric();
			x.get_rec_shape().setOutlineColor(sf::Color::Blue);
			x.get_rec_shape().setOutlineThickness(1.0f);
			m_board[i].push_back(x);
		}
	}
}
// copy shape to the board
void Grid::copy_shape(std::unique_ptr <Shapes> &shape)
{
	shape->get_rubrics_vec().at(0).get_rec_shape();
	for (int i = 0; i < 4; i++)
	{
		int col = convert(shape->get_rubrics_vec()[i].get_rec_shape().getGlobalBounds().left),
			row = convert(shape->get_rubrics_vec()[i].get_rec_shape().getGlobalBounds().top);
		if (m_board[row][col].get_is_empty())
			m_is_game_over = true;
		
			m_board[row][col].get_rec_shape().setFillColor(shape->get_color());
			m_board[row][col].set_is_empty(true);
	}
}

// colision with board rubrics
bool  Grid::shape_collision(std::unique_ptr <Shapes> &shape)
{
	return std::any_of(shape->get_rubrics_vec().begin(), shape->get_rubrics_vec().end(), [this](Rubric i) {
		int row = convert(i.get_rec_shape().getGlobalBounds().top),
			col = convert(i.get_rec_shape().getGlobalBounds().left);
		bool is_coli = false;
		if (row == SIZE - 1)
		{
			if (m_board[row][col].get_is_empty())
				is_coli = true;
		}
		else if (m_board[row + 1][col].get_is_empty())
			is_coli = true;
		return is_coli;
	});
}

// coilision with board rubrics in right
bool Grid::check_right(std::unique_ptr <Shapes> &shape)
{
	return std::any_of(shape->get_rubrics_vec().begin(), shape->get_rubrics_vec().end(),[this](Rubric i) {
		int row = convert(i.get_rec_shape().getGlobalBounds().top),
			col = convert((i.get_rec_shape().getGlobalBounds().left));

		return ((col != WIDTH - 1) && m_board[row][col + 1].get_is_empty());
	});
}

// coilision with board rubrics in left
bool Grid::check_left(std::unique_ptr <Shapes> &shape)
{
	return std::any_of(shape->get_rubrics_vec().begin(), shape->get_rubrics_vec().end(), [this](Rubric i) {
		int row = convert(i.get_rec_shape().getGlobalBounds().top),
			col = convert(i.get_rec_shape().getGlobalBounds().left);

		return ((col != 0) && m_board[row][col - 1].get_is_empty());
	});
}

// draw the board
void Grid::draw(sf::RenderWindow & window)
{
	for (int i = 0; i < LENGTH; i++)
		for (int j = 0; j < WIDTH; j++)
		{
			float pos_x = 0, pos_y = 0;
			pos_x = 10 + (j * SIZE);
			pos_y = 10 + (i * SIZE);
			m_board[i][j].get_rec_shape().setPosition(sf::Vector2f(pos_x, pos_y));
			window.draw(m_board[i][j].get_rec_shape());
		}	
}

// if row is full erase it and move the other rows
void Grid::check_row()
{
	auto deleteFrom = std::remove_if(m_board.begin(), m_board.end(), [](const std::vector <Rubric>& line) {
		return std::all_of(line.begin(), line.end(), [](const Rubric& rubric) { return rubric.get_is_empty(); });
	});

	m_board.erase(deleteFrom, m_board.end());
	int deleted = LENGTH - m_board.size();

	m_eaten_row = deleted; // for the controller

	m_board.insert(m_board.begin(), deleted, std::vector <Rubric>(WIDTH));

	for (int i = 0; i < deleted; i++)
	{
		for (int j = 0; j < WIDTH; j++)
		{	
			m_board[i][j].get_rec_shape().setOutlineColor(sf::Color::Blue);
			m_board[i][j].get_rec_shape().setOutlineThickness(1.0f);
		}
	}
}
// how many rows eaten 
int Grid::get_eaten_row() const
{
	return m_eaten_row;
}

void Grid::set_eaten_row(int eaten)
{
	m_eaten_row = eaten;
}
// is shape out of board
bool Grid::check_game_over()
{
	return m_is_game_over;
}
// help to calculate location
int Grid::convert(float num)
{
	return num/ SIZE;
}

Grid::~Grid()
{}
