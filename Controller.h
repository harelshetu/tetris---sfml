#pragma once
#include "Constants.h"
#include "Rubric.h"
#include "Grid.h"
#include "Shapes.h"
#include "Plus_Shape.h"
#include "P_Shape.h"
#include "R_shape.h"
#include "Q_Shape.h"
#include "L_Shape.h"
#include "Square_Shape.h"
#include "Line_Shape.h"
#include <SFML\Graphics.hpp>
#include <SFML/Audio.hpp>
#include <vector>
#include <memory>
#include <string> 
#include <sstream>


class Controller
{
public:
	Controller();
	void run();
	void rand_shape();
	void show_score(sf::Font const &font);
	void show_game_over(sf::Font const &font);
	~Controller();

private:
	sf::RenderWindow m_window;
	std::unique_ptr <Shapes> m_shape;
	Grid m_grid;
	int m_level;
	int m_score;
};

