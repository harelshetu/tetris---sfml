#include "P_Shape.h"

//constructor
P_Shape::P_Shape(sf::RenderWindow& window)
	:Shapes(window)
{
	for (int i = 0; i < m_rubrics.size(); i++)
		m_rubrics[i].get_rec_shape().setPosition(sf::Vector2f(100,40));// set the position for rotate

	m_rubrics[0].get_rec_shape().setOrigin(10, -10); // set all the origins
	m_rubrics[1].get_rec_shape().setOrigin(10, 10);
	m_rubrics[2].get_rec_shape().setOrigin(-10, 10);
	m_rubrics[3].get_rec_shape().setOrigin(-10, 30);
}



P_Shape::~P_Shape()
{
}
