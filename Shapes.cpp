#include "Shapes.h"

Shapes::Shapes(sf::RenderWindow & window) //constructor
	:m_window(window),m_is_locked(false)
{
	srand(time(NULL)); // renad a color for the shape

	int random = rand() % 5 + 1;
	switch (random)
	{
	case 1:
		m_color = sf::Color::Cyan;
		break;
	case 2:
		m_color = sf::Color::Green;
		break;
	case 3:
		m_color = sf::Color::Red;
		break;
	case 4:
		m_color = sf::Color::Yellow;
		break;
	case 5:
		m_color = sf::Color::Magenta;
		break;
	}
	for (int i = 0; i < NUM_OF_RUBRICS ; i++)
		m_rubrics.push_back(Rubric(m_color)); // make same color to all rubrics
}
// get the shape rubrics
  std::vector <Rubric>& Shapes::get_rubrics_vec() 
{
	return m_rubrics;
}
  // draw the shape
void Shapes::draw_shape()
{
	for (int i = 0; i < m_rubrics.size(); i++)
		m_window.draw(m_rubrics[i].get_rec_shape());
}
// move left the shape
void Shapes::move_left()
{
	if (std::any_of(m_rubrics.begin(), m_rubrics.end(), [this](Rubric i) {
		return (i.get_rec_shape().getGlobalBounds().top >= 410);
	}))
	{
		m_is_locked = true;
		return;
	}

	if (!m_is_locked)
	{
		if (std::any_of(m_rubrics.begin(), m_rubrics.end(), [this](Rubric i) {
			
			return (i.get_rec_shape().getGlobalBounds().left <= 10);
		}))
			return;

		for_each(m_rubrics.begin(), m_rubrics.end(), [this](Rubric &i)
		{i.get_rec_shape().move(-SIZE, 0); });
	}
}
// move the shape right
void Shapes::move_right()
{
	if (std::any_of(m_rubrics.begin(), m_rubrics.end(), [this](Rubric i) {
		return (i.get_rec_shape().getGlobalBounds().top + SIZE>= 410);
	}))
	{
		m_is_locked = true;
		return;
	}
	if (!m_is_locked)
	{

		if (std::any_of(m_rubrics.begin(), m_rubrics.end(), [this](Rubric i) {

			return (i.get_rec_shape().getGlobalBounds().left+SIZE >= 210);
		}))
			return;

		for_each(m_rubrics.begin(), m_rubrics.end(), [this](Rubric &i)
		{i.get_rec_shape().move(SIZE, 0); });
	}
}
// rotate the shape
void Shapes::rotate_shape()
{
	if (std::any_of(m_rubrics.begin(), m_rubrics.end(), [this](Rubric i) {
		return (i.get_rec_shape().getGlobalBounds().top + SIZE >= 410);
	}))
	{
		m_is_locked = true;
		return;
	}
	if (!m_is_locked)
	{
		bool u = false;
		for_each(m_rubrics.begin(), m_rubrics.end(), [this](Rubric &i)
		{i.get_rec_shape().rotate(90); });

		if (std::any_of(m_rubrics.begin(), m_rubrics.end(), [this](Rubric i) {
			sf::FloatRect pos = i.get_rec_shape().getGlobalBounds();
			return (pos.left < 10 || pos.left + SIZE > 210 || pos.top + SIZE > 410);
		}))
			for_each(m_rubrics.begin(), m_rubrics.end(), [this](Rubric &i)
		{i.get_rec_shape().rotate(-90); });
	}
}
// is shape reached the end
void Shapes::set_is_locked(bool locked)
{
	m_is_locked = locked;
}
// get color of the shape
sf::Color Shapes::get_color() const
{
	return m_color;
}
// move the shape down
void Shapes::move_down()
{
	for (int i = 0; i <  m_rubrics.size(); i++)
	{
		sf::FloatRect pos = m_rubrics[i].get_rec_shape().getGlobalBounds();
		if (pos.top +SIZE >= 410)
		{
			m_is_locked = true;
			return;
		}
	}

	for (int i = 0; i < m_rubrics.size(); i++)
	{
		sf::FloatRect pos = m_rubrics[i].get_rec_shape().getGlobalBounds();
		m_rubrics[i].get_rec_shape().move(0, SIZE);
	}
}
// is shape reached the end
bool Shapes::getIsLocked() const
{
	return m_is_locked;
}

Shapes::~Shapes()
{
}
