#include "Square_Shape.h"

//constructor
Square_Shape::Square_Shape(sf::RenderWindow& window)
	:Shapes(window)
{
	for (int i = 0; i < m_rubrics.size(); i++)
		m_rubrics[i].get_rec_shape().setPosition(sf::Vector2f(110, 30));// set the position for rotate

	m_rubrics[0].get_rec_shape().setOrigin(20, 0); // set all the origins
	m_rubrics[1].get_rec_shape().setOrigin(20, 20);
	m_rubrics[2].get_rec_shape().setOrigin(0, 20);
	m_rubrics[3].get_rec_shape().setOrigin(0, 0);
}


Square_Shape::~Square_Shape()
{
}
