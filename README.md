================================================================================


OOP - TETRIS
========================
by: Harel Shetu 

================================================================================

Description
===========
I built the classic Tetris game

================================================================================
classes
=======
1) Controller- controll all the managment of the game
2) Grid- drae the board 
3) Rubric- every shape contain vector of rubrics
4) Shapes- abstrucat. father of all shapes
5) L_Shape- Tetris Shape
6) Line_Shape- Tetris Shape
7) Plus_Shape- Tetris Shape
8) P_Shape- Tetris Shape
9) Square_Shape- Tetris Shape
10) Q_Shape- Tetris Shape
11) R_Shape- Tetris Shape

================================================================================

Data Structures
===============

I used d-que to store the board,

================================================================================

Known bugs
=========
none
================================================================================

