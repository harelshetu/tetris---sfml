#pragma once
#include <SFML\Graphics.hpp>
class Rubric
{
public:
	Rubric();
	Rubric(sf::Color);
	 sf::RectangleShape &get_rec_shape();
	sf::Color get_color() const;
	void set_is_empty(bool);
	bool get_is_empty() const;
	void draw(sf::RenderWindow&);
	~Rubric();

private:
	sf::Color m_color;
	sf::RectangleShape m_rec_shape;
	bool m_empty;
};

