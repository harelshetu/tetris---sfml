#pragma once
#include "Rubric.h"
#include "Constants.h"
#include <memory>
#include "Shapes.h"
#include <queue>

class Grid
{
public:
	Grid();
	void draw(sf::RenderWindow&);
	void copy_shape(std::unique_ptr <Shapes> &shape);
	bool shape_collision(std::unique_ptr <Shapes> &shape);
	bool check_right(std::unique_ptr <Shapes> &shape);
	bool check_left(std::unique_ptr <Shapes> &shape);
	void check_row();
	int get_eaten_row() const;
	void set_eaten_row(int);
	bool check_game_over();
	int convert(float);
	
	~Grid();

private:
	std::deque <  std::vector <Rubric>  > m_board;
	int m_eaten_row;
	bool m_is_game_over;
};

