#pragma once
#include <vector>
#include "Rubric.h"
#include "Constants.h"
#include <time.h>


class Shapes
{
public:
	Shapes(sf::RenderWindow&);
	void draw_shape();
	void move_left();
	void move_right();
	void rotate_shape();
	void move_down();
	bool getIsLocked() const;
	void set_is_locked(bool);
	sf::Color get_color() const;
	  std::vector <Rubric> &get_rubrics_vec() ;

	~Shapes();

protected:
	std::vector <Rubric> m_rubrics;
	sf::RenderWindow & m_window;
	sf::Color m_color;
	bool m_is_locked;
};

