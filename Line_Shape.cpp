#include "Line_Shape.h"

//constructor
Line_Shape::Line_Shape(sf::RenderWindow & window)
	:Shapes(window)
{
	for (int i = 0; i < m_rubrics.size(); i++)
		m_rubrics[i].get_rec_shape().setPosition(sf::Vector2f(110,30)); // set the position for rotate

	m_rubrics[0].get_rec_shape().setOrigin(20, 20); // set all the origins
	m_rubrics[1].get_rec_shape().setOrigin(40, 20);
	m_rubrics[2].get_rec_shape().setOrigin(0, 20);
	m_rubrics[3].get_rec_shape().setOrigin(-20, 20);
}

Line_Shape::~Line_Shape()
{
}
